from math import sin, cos, sqrt, pi, tan, log10


def moment_inertia(x):
    """
    Implements a n-dimensional translated sphere function for benchmarking optimization algorithms.
    The center of the sphere is at (1,2,3,...,n).

    :param x: point to be evaluated.
    :type x: numpy array of floats.
    :return: function value at x.
    :rtype: float.
    """

    density = 2.7*10**(-3)  # Density of the chipper material
    prof = 2*3.5   # Depth of one side of the chipper
    profMiddle = 57.6   # Depth of the middle of the chipper
    b = x[0]  # Total height
    h = x[1]  # Height from the top to the inclined area
    e = x[2]  # Thickness
    t = x[3]  # Angle
    r = 21.5  # Radius of the ball

    B = e / cos(t)
    m = B - e
    n = m/tan(t)
    H = b - h

    # Area of different parts of the chipper
    A2 = h * e
    A3 = n*m/2
    A4 = B * H
    A5 = (((0.6*H)**2)*tan(t))/2
    A6 = (0.6*H*tan(t) - B)*(0.6*H - e/sin(t))/2

    # Distance of the center of mass of the areas to the axis of rotation
    d3 = sqrt(((e + 2*B)/6)**2 + (h - n/3)**2)
    d4 = sqrt(((H * tan(t))/2 + B/2 - e/2)**2 + (b - H/2)**2)
    dBall = sqrt(((H - r*(1-sin(t)))*tan(t) + B - e/2 + r*cos(t))**2 + (b - r)**2)
    d5 = sqrt((H * tan(t) + B - e/2 - 0.6*H*tan(t)*2/3)**2 + (b - 0.6*H/3)**2)
    d6 = sqrt((H * tan(t) - e/2 - (0.6*H*tan(t)-B)*2/3) ** 2 + (b - (0.6 * H - e/sin(t)) / 3) ** 2)

    # Moments of inertia of each parts of the chipper
    moment1 = prof*(pi*e**4)/64
    moment2 = prof*(e*h*(e**2 + h**2)/12 + A2*(h/2)**2)
    moment3 = prof*(m*n*(m**2 + n**2)/36 + A3*d3**2)
    moment4 = prof*(B*H*(B**2 + (H**2)*((tan(t))**2 + 1))/12 + A4 * d4**2)
    momentBall = 2*46*(r**2)/5 + 46 * dBall**2
    moment5 = profMiddle*((((0.6*H)**2)*tan(t))*((0.6*H)**2 + (0.6*H*tan(t))**2)/36 + A5*d5**2)
    moment6 = profMiddle*((0.6*H*tan(t) - B)*(0.6*H - e/sin(t))*((0.6*H*tan(t) - B)**2 + (0.6*H - e/sin(t))**2)/36 + A6*d6**2)

    momentMiddle = moment5 - moment6

    # Moment of inertia of the chipper
    moment = (moment1 + moment2 + moment3 + moment4 + momentMiddle)*density + momentBall


    # Heuristics
    if b-h <= b*4/10:
        moment = moment + 100000
    if b > 21.5:
        moment = moment + 10000.0 * (b - 21.5)
    if b < 6:
        moment = moment + 10000.0 * (6.0 - b)
    if h < 3:
        moment = moment + 10000.0 * (3.0 - h)
    if e > 12:
        moment = moment + 10000.0 * (e - 12.0)
    if e < 4:
        moment = moment + 10000.0 * (4.0 - e)
    if t > pi/3:
        moment = moment + 100000.0 * (t - pi/3.0)
    if t < pi/8:
        moment = moment + 100000.0 * (1.0 + pi/8.0 - t)
    if moment <= 0 or b<=0:
        return 100000
    if moment/b > 100000:
        return 100000


    return moment/b


