import numpy as np
from simple_evolution_strategy import SimpleEvolutionStrategy
from funcao_custo import moment_inertia
import cma
from math import pi, inf
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Evolution Strategy algorithm (Simple ES or CMA-ES)
algorithm = 'cmaes'  # 'ses' or 'cmaes'
# Function which the optimization algorithm will optimize
function = moment_inertia
fig_format = 'png'  # 'svg' (Word), 'eps' (Latex), 'png' (best compatibility/worst quality)



# Simple ES parameters
m0 = np.random.uniform(np.array([5, 5, 2, pi/18]), np.array([18, 9, 10, pi/4]))  # initial guess used in the optimization algorithm
C0 = np.identity(4)  # initial covariance (SES)
mu = 75  # number of parents used for computing the mean and covariance of the next generation (SES)
population_size = 150  # population size (SES)
# CMA-ES parameters
sigma0 = 1  # initial step size (CMA-ES)

if algorithm == 'ses':
    es = SimpleEvolutionStrategy(m0, C0, mu, population_size)
else:
    es = cma.CMAEvolutionStrategy(m0, sigma0, {'popsize': 200})

num_iterations = 300
history_samples = []  # collect the samples of all iterations
history_fitnesses = []
best = [[], inf]
for i in range(num_iterations):
    samples = es.ask()
    # To avoid making the implementation of SES harder, I avoided following
    # the same interface of CMA-ES, thus I need to put an if here.
    if algorithm == 'ses':
        fitnesses = np.zeros(np.size(samples, 0))
        for j in range(np.size(samples, 0)):
            fitnesses[j] = function(samples[j, :])
        es.tell(fitnesses)
        history_samples.append(samples)
        for j in range(len(fitnesses)):
            if 0 < fitnesses[j] < best[1]:
                best = [samples[j], fitnesses[j]]

    else:
        fitnesses = [function(sample) for sample in samples]
        for j in range(len(fitnesses)):
            if 0 < fitnesses[j] < best[1]:
                best = [samples[j], fitnesses[j]]
            print(samples[j], fitnesses[j])
        for fitness in fitnesses:
            history_fitnesses.append(fitness)
        es.tell(samples, fitnesses)
        # reshaping samples to be a numpy matrix
        reshaped_samples = np.zeros((len(samples), np.size(samples[0])))
        for j in range(len(samples)):
            reshaped_samples[j, :] = samples[j]
        history_samples.append(reshaped_samples)

print("best: ", best)


