from math import pi
from funcao_custo import moment_inertia

'''b = float(input("Total height: "))
h = float(input("Height from the top to the inclined area: "))
e = float(input("Thickness: "))
t = float(input("Angle: "))'''

#Dimensions of the optimized chipper
b = 21.49
h = 10.9
e = 8
t = pi*40/180


#Dimensions of the current chipper
b2 = 21.49
h2 = 4.49
e2 = 8
t2 = pi/4

x = [b, h, e, t]
x2 = [b2, h2, e2, t2]
f = moment_inertia(x)
f2 = moment_inertia(x2)
r = (f2-f)*100/f2
moment = f*b

print("moment/b = {}".format(f))
print("moment = {}".format(moment))

print("Improvement of", r, "%")