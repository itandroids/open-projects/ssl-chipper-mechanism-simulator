Instructions for using the code:

the code has 5 .py files:
	- evolution_strategy
	- funcao_custo
	- moment_calculator
	- simple_evolution_strategy
	- benchmark_evolution_strategy

- To perform the optimization, just run the evolution_strategy file, choosing which optimization will be used (cmaes or ses).
- The implementation of the cost function was done in the funcao_custo file.
- To check the comparison between chippers, just run the moment_calculator file.
- To run the benchmark just run the benchmark_evolution_strategy.py file.